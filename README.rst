=================================================
Update Tryton packages in Guix
=================================================

This repository contains some script helping to update
`Tryton packages <https://hg.tryton.org>`_ in
`Guix <https://guix.gnu.org>`_.

:License: GNU Affero General Public License v3.0 or later
:SPDX-License-Identifier: AGPL-3.0-or-later


How-to
=================

.. note::

   This is a scratch! Depending on how steps succeed, one might need to go
   forth and back, repeat or change the order.

Collect what has to be done::

  VERSION=6.2

  ./fetch-modules-in-guix.sh
  ./fetch-modules-list.py $VERSION  # also generates task-lists

Update Existing Packages::

  git branch -c tryton-$VERSION
  ./update-existing-packages.sh | tee __required-input-changes.txt

  # check results
  wc -l _modules-to-update.txt
  git diff | grep -E '^\+.*version' | wc -l

  ./check-required-inputs-changes.sh
  less _required-inputs-changes.txt
  ./build-existing-packages.sh

Commit and squash the update commits::

  BASE_COMMIT=$(git rev-parse --short HEAD) # remember this commit
  (cd .. ; ./pre-inst-env guile etc/committer.scm)
  ./squash-update-commits.sh $VERSION $BASE_COMMIT

Import the new packages::

  ./import-new-packages.sh $VERSION
  ./fetch-descriptions.py $VERSION

  ./fix-pkgs.py $VERSION
  emacs _tryton-$VERSION/*.scm

  ./tryton-insert-pkg.py $VERSION
  ./build-new-packages.sh
  ./lint-new-packages.sh

Build all packages to validate::

  ./build-all-packages.sh


Common build errors and their solution
==========================================

* `assert len(records) == len(modules)`

  Validate that all module listed under `depends` in `tryton.cfg` (except `ir`
  and `res`) are listed as native-inputs.

* `trytond.exceptions.MissingDependenciesException:
  Missing dependencies: stock_shipment_cost`

  This might be a 'tests_require' (native-input) for another native-input.
  Validate that all module listed under `extra_depends` in `tryton.cfg` are
  listed as native-inputs.
