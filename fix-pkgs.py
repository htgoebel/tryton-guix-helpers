#!/usr/bin/env python
#
# Tryton Guix Helpers
# Copyright (C) 2022 Hartmut Goebel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later


import glob
import re
import os
import argparse

argparser = argparse.ArgumentParser()
argparser.add_argument('--force', action='store_true')
argparser.add_argument('version', help="Minor version we care of, e.g. 6.2")
args = argparser.parse_args()
del argparser

for fn in glob.iglob(f"_tryton-{args.version}/*.txt"):
    descname = os.path.splitext(fn)[0] + ".desc"
    outname = os.path.splitext(fn)[0] + ".scm"
    if not args.force and os.path.exists(outname):
        print("skipping", outname, "- already exists")
        continue
    print(outname)
    with open(fn) as fh:
        text = fh.read()
    with open(descname) as fh:
        desc = fh.read().strip()

    modname = os.path.splitext(os.path.basename(fn))[0]
    tpkgname = modname.replace("_", "-")
    pkgname = tpkgname.split('-', 1)[1]
    name = " ".join(l.capitalize() for l in pkgname.split("-"))
    text = re.sub(r'(base32|synopsis|description)\s+"', r'\1 "', text)
    text = re.sub(r'\(build-system python-build-system\)',
                  ('(build-system python-build-system)\n'
                   f'  (arguments (tryton-arguments "{modname}"))\n'
                   '   (native-inputs (%standard-trytond-native-inputs))'),
                  text)
    text = text.replace("python-trytond", "trytond")
    text = text.replace("python-psycopg2", "")
    text = re.sub(r'\(pypi-uri\s+("[^"]+")\s+version\)',
                  r'(pypi-uri \1 version)', text)
    text = re.sub(r'(\("[^"]+")\s+,', r'\1 ,', text)
    text = re.sub(r'\(license #f\)', '(license license:gpl3+)', text)
    text = re.sub(
        r'\(home-page "[^"]*"\)',
        f'(home-page "https://docs.tryton.org/projects/modules-{pkgname}")',
        text)
    text = '\n'.join(("  "+l).rstrip() for l in text.splitlines())
    text = re.sub(
        r'\(description ".*"\)',
        f'(description "The @emph{{{name}}} Tryton module provides\n{desc}")',
        text)
    text = f'(define-public {tpkgname}\n{text})'
    with open(outname, "w") as fh:
        fh.write(text)
