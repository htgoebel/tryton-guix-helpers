#!/usr/bin/env python
#
# Tryton Guix Helpers
# Copyright (C) 2022 Hartmut Goebel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import glob
import os
import requests
import lxml.html
import argparse

argparser = argparse.ArgumentParser()
argparser.add_argument('version', help="Minor version we care of, e.g. 6.2")
args = argparser.parse_args()
VERSION = args.version
del argparser

rs = requests.Session()

for fn in glob.iglob(f"_tryton-{VERSION}/*.txt"):
    outname = os.path.splitext(fn)[0] + ".desc"
    print(fn, end="")
    if os.path.exists(outname):
        print(" - already done")
        continue

    modname = os.path.splitext(os.path.basename(fn))[0]
    pkgname = modname.replace("_", "-")
    if pkgname.startswith('trytond-'):
        pkgname = pkgname[8:]
    homepage = f"https://docs.tryton.org/projects/modules-{pkgname}"
    r = rs.get(homepage)
    tree = lxml.html.document_fromstring(r.text)
    #breakpoint()
    desc = ''.join(tree.xpath("//h1/following-sibling::p[1]//text()"))
    with open(outname, "w") as fh:
        fh.write(desc)
    print(" - done")
    # break
