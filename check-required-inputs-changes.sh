#!/bin/bash
#
# Tryton Guix Helpers
# Copyright (C) 2022 Hartmut Goebel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later


# %standard-trytond-native-inputs
NATIVE_OKAY="dateutil genshi lxml magic passlib polib proteus relatorio sql
	     werkzeug wrapt"
NATIVE_OKAY="$(echo $NATIVE_OKAY)"
NATIVE_OKAY="python-(${NATIVE_OKAY// /|})"

INFILE=__required-inputs-changes.txt
OUTFILE=_required-inputs-changes.txt

grep consider "$INFILE" | \
    grep -vE " removing .* native .* $NATIVE_OKAY" | \
    grep -vE " adding .* propagated .* $NATIVE_OKAY" | \
    sed 's/^[^ ]*: //' | \
    sort > "$OUTFILE"
