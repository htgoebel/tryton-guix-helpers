#
# Tryton Guix Helpers
# Copyright (C) 2022 Hartmut Goebel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# stock_package_shipping_sendcloud
# sale-credit-limit
# sale

import glob
import re
import os
import subprocess
import argparse


argparser = argparse.ArgumentParser()
argparser.add_argument('version', help="Minor version we care of, e.g. 6.2")
args = argparser.parse_args()
VERSION = args.version
del argparser


HERE = os.path.abspath(os.path.dirname(__file__))
os.chdir('..')


TRYTON_SCM_FILE = "gnu/packages/tryton.scm"


def read_tryton_scm():
    with open(TRYTON_SCM_FILE) as fh:
        text = fh.read()
    HEADER, p, modules_text = text.partition(
        "(define-public trytond-account")
    modules_text = (p + modules_text).strip()
    modules = re.split(r"(\(define-public (.+))", modules_text)
    if modules[0] == "":
        modules.pop(0)
    assert len(modules) % 3 == 0
    MODULES = {}
    for p, name, body in zip(*(iter(modules),) * 3):
        MODULES[name.strip()] = (p + body).strip()
    return HEADER.strip(), MODULES


HEADER, MODULES = read_tryton_scm()


def pkg_key(x):
    if x.startswith("python-"):
        return (x[7:], 1)
    else:
        return (x, 0)


for fn in sorted(glob.iglob(os.path.join(HERE, f"_tryton-{VERSION}/*.scm"))):
    print(fn)
    with open(fn) as fh:
        text = fh.read().strip()
    modname = os.path.basename(fn).split(".scm")[0]
    pkgname = modname.replace("_", "-")
    print(fn, pkgname, sep="\t")
    assert pkgname not in MODULES, pkgname
    # continue
    MODULES[pkgname] = text
    # continue
    with open(TRYTON_SCM_FILE, "w") as fh:
        print(HEADER, file=fh)
        for m in sorted(MODULES.keys(), key=pkg_key):
            print(file=fh)
            print(MODULES[m], file=fh)
    subprocess.check_call(["./pre-inst-env", "guile", "etc/committer.scm"])
    # break

if False:
    for m in sorted(MODULES.keys(), key=pkg_key):
        print(m)
