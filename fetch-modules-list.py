#!/usr/bin/env python
#
# Tryton Guix Helpers
# Copyright (C) 2022 Hartmut Goebel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
"""
Fetch list of all modules and their version from https://downloads.tryton.org/
"""

import requests
import lxml.html

import pkg_resources
import argparse

argparser = argparse.ArgumentParser()
argparser.add_argument('version', help="Minor version we care of, e.g. 6.2")
args = argparser.parse_args()
VERSION = args.version
del argparser


SKIP = [
    # 'trytond_gis',
    # 'trytond_google_maps',
    'trytond_stock_package_shipping_dpd',
    ]

TRYTON_BASEURL = 'https://downloads.tryton.org/{version}'
TRYTON_MODULELIST = '{TRYTON_BASEURL}/modules.txt'

NEW = []
UPDATE = []
MODULES = []


def get_module_list(version):
    r = requests.get(TRYTON_BASEURL.format(version=version))
    tree = lxml.html.document_fromstring(r.text)
    files = tree.xpath("//a/@href")
    files = (fn[:-7] for fn in files if fn.endswith('.tar.gz'))
    modules = {}
    for i, fn in enumerate(files):
        m = fn.split('-', 1)
        n = m[0]
        v = pkg_resources.parse_version(m[1])
        if n not in modules:
            modules[n] = v
        elif modules[n] < v:
            modules[n] = v
    return modules


with open('_modules-in-guix.txt') as fh:
    guix_modules = set(l.rstrip() for l in fh.readlines())  # noqa: E741

modules = get_module_list(VERSION)
for n, v in sorted(modules.items()):
    gn = n.replace('_', '-')
    if n == 'proteus':
        gn = 'python-proteus'
    line = (n, gn, v)
    MODULES.append(line)
    if gn in guix_modules:
        UPDATE.append(line)
    elif gn not in SKIP and n not in SKIP:
        NEW.append(line)

with open(f'_modules-{VERSION}.txt', 'w') as fh:
    for line in MODULES:
        print(*line, sep='\t', file=fh)
    print(fh.name, "created")

with open('_modules-to-update.txt', 'w') as fh:
    for line in UPDATE:
        print(*line, sep='\t', file=fh)
    print(fh.name, "created")

with open('_modules-to-import.txt', 'w') as fh:
    for line in NEW:
        print(*line, sep='\t', file=fh)
    print(fh.name, "created")
