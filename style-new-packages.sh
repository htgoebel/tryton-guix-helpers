#!/bin/bash
#
# Tryton Guix Helpers
# Copyright (C) 2022 Hartmut Goebel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

cat _modules-to-import.txt | \
    while read name guixname version ; do
	if [ ${name/[-#]//} != $name ] ; then
	    echo >&2 "skipping $name"
	    continue
	fi
	#echo >&2 $guixname
	#../pre-inst-env guix refresh -u $guixname@$version
	echo $guixname@$version
    done \
	| xargs ../pre-inst-env guix style -S inputs
