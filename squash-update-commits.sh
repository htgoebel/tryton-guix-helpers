#!/bin/bash
#
# Tryton Guix Helpers
# Copyright (C) 2022 Hartmut Goebel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Usage:
#   ./squash-update-commits.sh VERSION BASE_COMMIT [--test]
#
# BASE_COMMIT is the commit *before* the first one to be squashed.
#

VERSION=${1:?version must be given}
COMMIT=${2:?base commit must be given}
GENERATE=${3}

if [ "$GENERATE" = "--test" ] ; then
    TEST=1
elif [ "$GENERATE" = "generate-commit-message" ] ; then
    echo "gnu: tryton applications and framework: Update to $VERSION.x."
    echo
    git log --format='%b####---###---###' --reverse $COMMIT.. | python3 -c '

import sys, textwrap, re
PATTERN = r"\* gnu/packages/tryton.scm \((.*?)\)(.*?)####---###---###"
#PATTERN = r"gnu/packages/tryton\.scm .*####---###---###"
#PATTERN = re.compile(PATTERN, re.M))

updates = {}
text = sys.stdin.read()
#print(text, file=sys.stderr)
i = -1
for i, (name, msg) in enumerate(re.findall(PATTERN, text, re.DOTALL)):
    msg = " ".join(msg.split())
    updates.setdefault(msg.strip(), []).append(name)
print(i + 1, "entries found", file=sys.stderr)

prefix = "* gnu/packages/tryton.scm "
for msg, names in sorted(updates.items()):
    names = ", ".join(names)
    line = f"({names}){msg}"
    lines = textwrap.wrap(line, width=78, break_on_hyphens=False,
                          initial_indent=prefix, subsequent_indent="  ")
    print(*lines, sep="\n")
    prefix = "  "
'
    exit
fi

MESSAGE="$($(realpath $0) $VERSION $COMMIT generate-commit-message)"
if [ -n "$TEST" ] ; then
    echo "$MESSAGE" | less
else
    git reset --soft $COMMIT &&
	git commit --edit -m"$MESSAGE"
fi
